sudo docker run -d --name=dev-consul -e CONSUL_BIND_INTERFACE=eth0 -p 8500:8500 consul:1.2.2

docker run -d -e CONSUL_BIND_INTERFACE=eth0 consul agent -dev -join=172.17.0.2

-advertise(advertise_addr)   在集群中，节点之间相互通知的地址，默认情况下使用-bind绑定的地址

-advertise-wan(advertise_addr_wan)    一个集群与另一个集群的node进行通信的地址

-bootstrap(bootstrap)     指的是只有一个server节点的datacenter,在这种模式下这个节点可以自选举为leader

-bootstrap-expect(bootstrap_expect)   集群至少需要的server数,只有加入到这个集群中的server达到这个数量的时候才会选举leader


-bind(bind_addr)     集群内部交流地址，默认0.0.0.0指的是本机ip


-serf-wan-bind(serf_wan_bind)    默认使用-bind地址，用于不同集群广域网交流

-serf-lan-bind(serf_lan_bind)    默认使用-bind地址，用于局域网内部节点交流信息


-client(client_addr)   agent以client模式运行，默认使用127.0.0.1地址。


-config-file     加载一个配置文件


-config-dir     加载配置文件所在的目录，该目录下所有以.json结尾的都会被加载


-config-format   更改加载的配置文件后缀名


-data-dir(data_dir)        保存节点运行中的一些状态信息的目录


-datacenter(datacenter)      集群的名字，默认是dc1吗，一个集群应该运行在一个局域网当中


-dev         一种开发模式，运行中不会有任何信息保存到硬盘中，


-disable-host-node-id(disable_host_node_id)     主动赋予一个node一个自定义的node id而不是自动生成的随机数作为id

-disable-keyring-file(disable_keyring_file)      k/v值被保存到一个文件中，如果是false将关闭以后消失


-dns-port               dns   api运行的接口  默认8600


-domain(domain)

-enable-script-checks(enable_script_checks)       在这节点上执行健康检查是否允许使用脚本,默认是false,如果设置为true最好设置enable acl，这样可以注册的时候允许使用脚本


-encrypt(encrypt)     支出集群中用来加密解密的秘钥，一个集群中只能使用一个秘钥，最好使用consul keygen生成该秘钥

-hcl      加载配置文件的位置


-http-port         http协议监听接口默认8500


-join         启动的时候加入到另一个节点的地址组成集群，如果加入不进去就会启动失败


-retry-join(retry_join)      类似于join，但是多了重试机制，就是加入失败以后还会重试


-retry-interval(retry_interval)    加入集群超时时间，默认30s


-retry-max       -join加入集群最大重试次数，默认是0


-join-wan        加入到广域网另一个集群里面的地址,加入失败将会启动失败


-retry-join-wan(retry_join_wan)     加入广域网集群失败重试


-retry-interval-wan(retry_interval_wan)    加入广域网集群间隔时间


-retry-max-wan       加入广域网集群重试次数


-log-level           "trace", "debug", "info", "warn", and "err"日志级别默认info


-node             集群中一个节点的名字，集群中必须唯一


-node-id         节点不可变的标示，-node重启以后可以改变，但是这个不能改变, -disable-host-node-id要是设置为true了，就不会有了


-node-meta


-pid-file       保存pid信息文件地址


-protocol(protocol)         使用的consul   协议版本，默认是最新的  consul -v

-raft-protocol(raft_protocol)     raft内部版本协议


-recursor       dns上游服务器地址


-rejoin(rejoin_after_leave)       以前离开集群的节点重新加入集群中


-segment(segment)       集群内agent分区，默认为空串，即所有集群节点都在这个默认的分区中。


-server(server)      节点运行server模式，拥有选举权和保存、交换服务信息的地方

-non-voting-server(non_voting_server)     该server不参与raft选举，用来处理高并发集群中的只读server


-syslog(enable_syslog)      Linux and OSX ,不能在windows上设置，

-ui(ui)          运行ui

-ui-dir(ui_dir)      ui文件所在的目录,不能与-ui一同设置