package io.framework.discovery.consul.config;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
/**
 * <B>文件描述：</B><BR>
 * <BR>
 * <B>版权声明：</B>(C)2017-2019<BR>
 * <B>创建时间：</B>18-7-31<BR>
 *
 * @author mifan gaozhenpeng@aliyun.com
 * @version v1.0.1
 * @since JDK 1.8
 */
@Configuration
@EnableDiscoveryClient
@EnableScheduling
public class ConsulConfig {
}
