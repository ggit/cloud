package io.framework.modules.utils;

/**
 * 代码安全审计系统 <br/>
 * <B>文件描述：</B><BR>
 * <BR>
 * <B>版权声明：</B>(C)2017-2019<BR>
 * <B>公司部门：</B>NERCIS 关键技术实验室 <BR>
 * <B>创建时间：</B>18-7-31<BR>
 *
 * @author mifan gaozhp@nercis.ac.cn
 * @version v1.0.1
 * @since JDK 1.8
 */
public  class StringUtils {

    public StringUtils() throws Exception {
        throw new  Exception("不能初始化");
    }

    public static boolean isBank(final String str){
        return org.apache.commons.lang3.StringUtils.isBlank(str);
    }
}
